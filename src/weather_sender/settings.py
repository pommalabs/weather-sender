# Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

from functools import cache

from dolcetto.settings import get_settings_config
from pydantic import AnyHttpUrl
from pydantic.fields import Field
from pydantic_settings import BaseSettings, SettingsConfigDict


class OpenWeatherSettings(BaseSettings):
    """OpenWeather is the service from which weather forecasts are downloaded."""

    model_config = get_settings_config(SettingsConfigDict(env_prefix="openweather_"))

    api_key: str = Field(
        description="API key used to get weather data with OpenWeather."
    )
    base_url: AnyHttpUrl = Field(
        default=AnyHttpUrl("https://api.openweathermap.org"),
        description="OpenWeather API base URL.",
    )
    timeout: int = Field(
        default=30,
        description="How many seconds should be used as timeout for OpenWeather"
        " API HTTP requests",
    )

    @staticmethod
    @cache
    def load() -> "OpenWeatherSettings":
        return OpenWeatherSettings()  # type: ignore


class TelegramSettings(BaseSettings):
    """Telegram is used to send messages with weather forecasts."""

    model_config = get_settings_config(SettingsConfigDict(env_prefix="telegram_"))

    bot_token: str = Field(description="Bot token used to send messages with Telegram.")
    disable_notification: bool = Field(
        default=True,
        description="Controls whether sent messages should trigger a full notification."
        " Even if set to false, only the first message will trigger a full notification."
        " Defaults to true, which means that messages will trigger a silent notification.",
    )

    @staticmethod
    @cache
    def load() -> "TelegramSettings":
        return TelegramSettings()  # type: ignore


class HealthCheckSettings(BaseSettings):
    model_config = get_settings_config(SettingsConfigDict(env_prefix="health_check_"))

    probe_timeout: int = Field(
        default=2,
        description="How many seconds should OpenWeather API probe last before failing."
        " Default value is 2 seconds, timeout value should be greater than zero.",
        gt=0,
    )

    @staticmethod
    @cache
    def load() -> "HealthCheckSettings":
        return HealthCheckSettings()
