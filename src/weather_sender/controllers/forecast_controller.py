# Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

import logging
from json import JSONDecodeError

from dolcetto.services import SmtpClient
from litestar import Controller, Response, post
from litestar.background_tasks import BackgroundTask, BackgroundTasks
from litestar.exceptions import HTTPException, NotAuthorizedException
from litestar.status_codes import HTTP_200_OK, HTTP_424_FAILED_DEPENDENCY

from weather_sender.models import (
    ForecastRequest,
    ForecastRequests,
    ForecastResult,
    ForecastResults,
    ForecastSourceData,
    ForecastTransformedData,
)
from weather_sender.services import (
    get_air_pollution_data,
    get_location_data,
    get_weather_data,
    send_error_email,
    send_telegram_message,
    transform_weather_data,
)
from weather_sender.settings import OpenWeatherSettings, TelegramSettings


class ForecastController(Controller):
    path = "/forecasts"
    tags = ["weather_sender"]

    # pylint: disable=too-many-arguments,too-many-positional-arguments
    @post(
        "/send-one",
        status_code=HTTP_200_OK,
        raises=[NotAuthorizedException],
        operation_id="send_one",
    )
    async def send_one_weather_forecast(
        self,
        data: ForecastRequest,
        logger: logging.Logger,
        openweather_settings: OpenWeatherSettings,
        telegram_settings: TelegramSettings,
        smtp_client: SmtpClient | None,
    ) -> ForecastResult:
        """Sends a weather forecast for given location to specified Telegram chat ID."""

        return await ForecastController._do_send_one_weather_forecast(
            data, logger, openweather_settings, telegram_settings, smtp_client
        )

    # pylint: disable=too-many-arguments,too-many-positional-arguments
    @post(
        "/send-many",
        status_code=HTTP_200_OK,
        raises=[NotAuthorizedException],
        operation_id="send_many",
    )
    async def send_many_weather_forecasts(
        self,
        data: ForecastRequests,
        logger: logging.Logger,
        openweather_settings: OpenWeatherSettings,
        telegram_settings: TelegramSettings,
        smtp_client: SmtpClient | None,
    ) -> Response[ForecastResults]:
        """Sends multiple weather forecasts."""

        logger.info("Requests: %d", len(data.requests))
        logger.info("Background: %s", data.background)

        results = []
        background_tasks: list[BackgroundTask] = []

        for request in data.requests:
            if data.background:
                ForecastController.add_background_task(
                    background_tasks,
                    BackgroundTask(
                        ForecastController._do_send_one_weather_forecast,
                        request,
                        logger,
                        openweather_settings,
                        telegram_settings,
                        smtp_client,
                    ),
                )
            else:
                results.append(
                    await ForecastController._do_send_one_weather_forecast(
                        request,
                        logger,
                        openweather_settings,
                        telegram_settings,
                        smtp_client,
                    )
                )

        return Response(
            ForecastResults(results=results),
            background=BackgroundTasks(background_tasks),
        )

    @staticmethod
    async def _do_send_one_weather_forecast(
        request: ForecastRequest,
        logger: logging.Logger,
        openweather_settings: OpenWeatherSettings,
        telegram_settings: TelegramSettings,
        smtp_client: SmtpClient | None,
    ) -> ForecastResult:
        logger.info("Coordinates: (%f, %f)", request.latitude, request.longitude)
        logger.info("Language: %s", request.language)
        logger.info("Telegram chat ID: %d", request.chat_id)

        weather_data = None
        try:
            # Retrieve location, weather and air pollution data
            # from OpenWeather REST services.
            source_location_data = get_location_data(
                openweather_settings,
                request.latitude,
                request.longitude,
            )[0]
            source_weather_data = get_weather_data(
                openweather_settings,
                request.latitude,
                request.longitude,
                request.language,
                request.units,
            )
            try:
                source_air_pollution_data = get_air_pollution_data(
                    openweather_settings,
                    request.latitude,
                    request.longitude,
                )
            except JSONDecodeError:
                source_air_pollution_data = {}

            # Transform weather data according to our needs.
            weather_data = transform_weather_data(
                request,
                source_weather_data,
                source_location_data,
                source_air_pollution_data,
                logger,
            )

            # Prepare and send the message to the specified Telegram chat ID.
            await send_telegram_message(
                telegram_settings, request.chat_id, weather_data
            )

            # Return all used data to the caller.
            source_data = ForecastSourceData(
                weather=source_weather_data,
                location=source_location_data,
                air_pollution=source_air_pollution_data,
            )
            transformed_data = ForecastTransformedData(weather=weather_data)
            return ForecastResult(
                source_data=source_data, transformed_data=transformed_data
            )
        except Exception as ex:
            logger.error(
                "An error occurred while sending forecast for coordinates: (%f, %f)",
                request.latitude,
                request.longitude,
                exc_info=True,
            )
            send_error_email(smtp_client, logger, request, ex)
            raise HTTPException(status_code=HTTP_424_FAILED_DEPENDENCY) from ex

    @staticmethod
    def add_background_task(tasks: list[BackgroundTask], task: BackgroundTask) -> None:
        # Implemented in order to be able to mock background tasks.
        tasks.append(task)
