# Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

from dolcetto import (
    API_KEY_SECURITY_SCHEME,
    API_KEY_SECURITY_SCHEME_NAME,
    APP_DESCRIPTION,
    APP_VERSION,
    MIT_LICENSE,
    ApiKeyAuthenticationMiddleware,
    AppInfoController,
    HttpTracingMiddleware,
    logging_exception_handler,
    on_app_init,
    redirect_to_docs,
)
from litestar import Litestar, Router
from litestar.di import Provide
from litestar.openapi import OpenAPIConfig
from litestar.openapi.spec import Components
from litestar.status_codes import HTTP_500_INTERNAL_SERVER_ERROR

from weather_sender.controllers import ForecastController
from weather_sender.services import get_health_check
from weather_sender.settings import (
    HealthCheckSettings,
    OpenWeatherSettings,
    TelegramSettings,
)

root = Router(
    path="/",
    route_handlers=[redirect_to_docs, AppInfoController],
    dependencies={
        "health_check_settings": Provide(
            HealthCheckSettings.load, use_cache=True, sync_to_thread=False
        ),
        "health_check": Provide(get_health_check, use_cache=True, sync_to_thread=False),
    },
)

restricted_api_v1 = Router(
    path="/api/v1",
    route_handlers=[ForecastController],
    security=[{API_KEY_SECURITY_SCHEME_NAME: []}],
    middleware=[
        ApiKeyAuthenticationMiddleware.define(),
        HttpTracingMiddleware.define(),
    ],
    exception_handlers={HTTP_500_INTERNAL_SERVER_ERROR: logging_exception_handler()},
    dependencies={
        "telegram_settings": Provide(
            TelegramSettings.load, use_cache=True, sync_to_thread=False
        ),
    },
)

app = Litestar(
    openapi_config=OpenAPIConfig(
        title="Weather Sender",
        description=APP_DESCRIPTION,
        version=APP_VERSION,
        license=MIT_LICENSE,
        components=Components(
            security_schemes={API_KEY_SECURITY_SCHEME_NAME: API_KEY_SECURITY_SCHEME}
        ),
    ),
    route_handlers=[root, restricted_api_v1],
    on_app_init=[on_app_init],
    dependencies={
        "openweather_settings": Provide(
            OpenWeatherSettings.load, use_cache=True, sync_to_thread=False
        ),
    },
)
