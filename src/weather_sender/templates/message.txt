{%- if daily[0].prec and daily[0].prec >= 0.2 %}
{{ "prec" | transl(lang) }}: <b>{{ daily[0].prec | numfmt(1) }} mm</b>, <b>{{ (daily[0].pop * 100) | numfmt(0) }}%</b> {{ "prec_probability" | transl(lang) }}
{%- elif daily[0].prec and daily[0].prec >= 0.1 %}
{{ "prec" | transl(lang) }}: <b>{{ "no_or_light_prec" | transl(lang) }}</b>
{%- else %}
{{ "prec" | transl(lang) }}: <b>{{ "no_prec" | transl(lang) }}</b>
{%- endif %}
{{ "temp" | transl(lang) }}: <b>{{ daily[0].temp_max | numfmt(0) }} / {{ daily[0].temp_min | numfmt(0) }} {{ "°C" if units == "metric" else "°F" }}</b>
{{ "humidity" | transl(lang) }}: {{ "humidity_higher_than" | transl(lang) }} <b>{{ daily[0].humidity }}%</b>
{{ "wind" | transl(lang) }}: {{ "wind_speed_up_to" | transl(lang) }} <b>{{ (daily[0].wind_gust if daily[0].wind_gust else daily[0].wind_speed) | numfmt(0) }} {{ "km/h" if units == "metric" else "mph" }}</b> {{ "wind_from" | transl(lang) }} <b>{{ daily[0].wind_direction | transl(lang) }}</b>

{% for alert in alerts -%}
{{ alert.icon }} <b>{{ alert.event }}</b>
{{ "alert_validity" | transl(lang) }}: <b>{{ alert.start_day }} {{ alert.start_hour }}:{{ alert.start_minute }}</b> –⁠ <b>{{ alert.end_day }} {{ alert.end_hour }}:{{ alert.end_minute }}</b>
{{ "alert_source" | transl(lang) }}: {{ alert.sender_name | transl(lang) }}

{% endfor %}