# Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

import functools
import logging

from dolcetto.services import probe_url
from healthcheck import HealthCheck
from litestar.status_codes import HTTP_503_SERVICE_UNAVAILABLE

from weather_sender.settings import HealthCheckSettings, OpenWeatherSettings


def probe_openweather_api(
    logger: logging.Logger,
    health_check_settings: HealthCheckSettings,
    openweather_settings: OpenWeatherSettings,
) -> tuple[bool, str]:
    return probe_url(
        openweather_settings.base_url, logger, health_check_settings.probe_timeout
    )


def get_health_check(
    logger: logging.Logger,
    health_check_settings: HealthCheckSettings,
    openweather_settings: OpenWeatherSettings,
) -> HealthCheck:
    health_check = HealthCheck(failed_status=HTTP_503_SERVICE_UNAVAILABLE)

    check = functools.partial(
        probe_openweather_api, logger, health_check_settings, openweather_settings
    )
    setattr(check, "__name__", probe_openweather_api.__name__)
    health_check.add_check(check)

    return health_check
