# Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

import logging
import tempfile
import traceback
from typing import Iterable

import telegram
from dolcetto.models import MailMessage
from dolcetto.services import SmtpClient

from weather_sender.models import ForecastRequest
from weather_sender.services.templating import (
    AIR_TEMPLATE,
    MESSAGE_TEMPLATE,
    TODAY_TEMPLATE,
    TONIGHT_TEMPLATE,
    WEEK_TEMPLATE,
    convert_html_to_image,
    render_template,
)
from weather_sender.settings import TelegramSettings


def _create_tmp_html() -> tempfile._TemporaryFileWrapper:
    return tempfile.NamedTemporaryFile(mode="w", encoding="utf-8", suffix=".html")


def _create_tmp_image() -> tempfile._TemporaryFileWrapper:
    return tempfile.NamedTemporaryFile(suffix=".jpg")


async def _send_telegram_message(
    telegram_settings: TelegramSettings, chat_id: int, message: str, images: Iterable
) -> None:
    bot = telegram.Bot(token=telegram_settings.bot_token)
    # Default read and write timeout is 5 seconds,
    # but that was increased to address transient errors.
    rw_timeout = 30
    await bot.send_message(
        chat_id,
        text=message,
        parse_mode="HTML",
        disable_notification=telegram_settings.disable_notification,
        read_timeout=rw_timeout,
        write_timeout=rw_timeout,
    )
    await bot.send_media_group(
        chat_id,
        media=[telegram.InputMediaPhoto(img) for img in images],
        # Only the first message should trigger a notification.
        disable_notification=True,
        read_timeout=rw_timeout,
        write_timeout=rw_timeout,
    )


async def send_telegram_message(
    telegram_settings: TelegramSettings, chat_id: int, weather_data: dict
) -> None:
    # Prepare Telegram text message.
    message = render_template(MESSAGE_TEMPLATE, weather_data)

    # Prepare forecast images and send them with the text message.
    with (
        _create_tmp_html() as today_html,
        _create_tmp_image() as today_image,
        _create_tmp_html() as tonight_html,
        _create_tmp_image() as tonight_image,
        _create_tmp_html() as week_html,
        _create_tmp_image() as week_image,
        _create_tmp_html() as air_html,
        _create_tmp_image() as air_image,
    ):
        for tpl in [
            (TODAY_TEMPLATE, today_html, today_image),
            (TONIGHT_TEMPLATE, tonight_html, tonight_image),
            (WEEK_TEMPLATE, week_html, week_image),
            (AIR_TEMPLATE, air_html, air_image),
        ]:
            html = render_template(tpl[0], weather_data)
            convert_html_to_image(html, tpl[1], tpl[2])

        images = [today_image, tonight_image, week_image, air_image]
        await _send_telegram_message(telegram_settings, chat_id, message, images)


def send_error_email(
    smtp_client: SmtpClient | None,
    logger: logging.Logger,
    request: ForecastRequest,
    exception: Exception,
) -> None:
    if smtp_client is None or len(smtp_client.settings.default_to_addresses) == 0:
        logger.warning(
            "Error email could not be sent, not all required SMTP settings"
            " have been configured properly or default recipient addresses have not been set."
        )
        return

    subject = f"Weather forecast for ({request.latitude}, {request.longitude}) could not be sent"

    body = (
        "An error occurred while sending weather forecast "
        f"for ({request.latitude}, {request.longitude}), "
        f"with language set to '{request.language}', "
        f"units of measurement set to '{request.units.value}', "
        f"custom location name set to '{request.location_name}' "
        f"and Telegram chat ID set to '{request.chat_id}'.\n\n"
    )

    exception_details = traceback.format_exception(exception)
    for exception_detail in exception_details:
        body += exception_detail

    mail_message = MailMessage(
        subject=subject,
        plain_text_content=body,
    )

    with smtp_client.open_connection() as smtp_conn:
        smtp_conn.send_mail(mail_message)
