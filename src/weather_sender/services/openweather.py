# Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

import json
import logging
from datetime import date, datetime
from typing import cast
from zoneinfo import ZoneInfo

import backoff
import httpx
from babel.dates import format_date

from weather_sender.models import (
    AlertDatum,
    DailyDatum,
    ForecastRequest,
    HourlyDatum,
    Units,
)
from weather_sender.services.translations import translate_alert_event
from weather_sender.settings import OpenWeatherSettings

MAX_HTTP_TRIES = 5
"""How many times HTTP calls to OpenWeather APIs should be retried on error."""

WIND_DIRECTIONS = [
    "N",
    "NNE",
    "NE",
    "ENE",
    "E",
    "ESE",
    "SE",
    "SSE",
    "S",
    "SSW",
    "SW",
    "WSW",
    "W",
    "WNW",
    "NW",
    "NNW",
]
"""List of wind directions (e.g. "N", "SW", etc.)."""

AIR_POLLUTION_COMPONENTS = [
    "co",
    "no2",
    "o3",
    "so2",
    "pm2_5",
    "pm10",
]
"""Air pollution components which are considered into our report."""


def _convert_degrees_to_direction(angle: float) -> str:
    # Divides the angle by 22.5 because 360°/16 directions yields 22.5° per direction change.
    # Also adds 0.5 so that when value is truncated,
    # the "tie" between the change threshold can broken.
    # Then, truncate the value using integer division (so there is no rounding).
    index = int((angle / 22.5) + 0.5)
    return WIND_DIRECTIONS[(index % 16)]


def _convert_timestamp_to_local(timestamp: int, timezone: ZoneInfo) -> datetime:
    """Converts a UNIX timestamp into local date/time."""
    return datetime.fromtimestamp(timestamp, timezone)


def _convert_wind_speed(wind_speed: float, units: Units) -> int:
    """Converts given wind speed from m/s to km/h when units are set to 'metric'.

    If units are set to 'imperial', then wind speed is not converted.
    """
    if units == Units.IMPERIAL:
        return round(wind_speed)
    return round(wind_speed * 3.6) if wind_speed is not None else 0


def _get_current_date(weather_data: dict, timezone: ZoneInfo) -> date:
    first_hour = weather_data["hourly"][0]
    local_hour = _convert_timestamp_to_local(first_hour["dt"], timezone)
    return local_hour.date()


def _get_formatted_date(local_date: datetime, lang: str) -> str:
    return format_date(local_date, format="EEE d/M", locale=lang)


def _get_daily_prec(day: dict) -> float | None:
    rain = cast(float, day["rain"] if "rain" in day else 0.0)
    snow = cast(float, day["snow"] if "snow" in day else 0.0)
    prec = rain + snow
    return prec if prec > 0 else None


def _get_daily_prec_from_hourly_data(hourly_data: list[dict]) -> float | None:
    today_hourly_prec = map(_get_hourly_prec, hourly_data)
    prec = sum(map(lambda p: float(p if p else 0.0), today_hourly_prec))
    return prec if prec > 0 else None


def _get_hourly_prec(hour: dict) -> float | None:
    rain = cast(float, hour["rain"]["1h"] if "rain" in hour else 0.0)
    snow = cast(float, hour["snow"]["1h"] if "snow" in hour else 0.0)
    prec = rain + snow
    return prec if prec > 0 else None


def _get_hourly_data_for_today(weather_data: dict, timezone: ZoneInfo) -> list[dict]:
    first_hour = weather_data["hourly"][0]
    local_hour = _convert_timestamp_to_local(first_hour["dt"], timezone)
    current_date = local_hour.date()
    today_hourly_data = []
    for hour in weather_data["hourly"]:
        local_hour = _convert_timestamp_to_local(hour["dt"], timezone)
        if local_hour.date() == current_date:
            today_hourly_data.append(hour)
    return today_hourly_data


def _transform_hourly_data(
    weather_data: dict, timezone: ZoneInfo, units: Units
) -> dict:
    today: list[HourlyDatum] = []
    tonight: list[HourlyDatum] = []

    # Current date is needed to divide hourly forecast for today and tonight.
    current_date = _get_current_date(weather_data, timezone)

    # Each page can contain up to 15 rows.
    max_row_count = 15

    for index, hour in enumerate(weather_data["hourly"]):
        if index >= 24:
            # We want to handle only the next 24 hours.
            break

        local_hour = _convert_timestamp_to_local(hour["dt"], timezone)
        weather = hour["weather"][0]

        hourly_datum = HourlyDatum()
        hourly_datum.hour = local_hour.strftime("%H")
        hourly_datum.humidity = hour["humidity"]
        hourly_datum.prec = _get_hourly_prec(hour)
        hourly_datum.temp = hour["temp"]
        hourly_datum.uvi = hour["uvi"]
        hourly_datum.weather_description = weather["description"]
        hourly_datum.weather_icon = weather["icon"]
        hourly_datum.wind_speed = _convert_wind_speed(hour["wind_speed"], units)

        if local_hour.date() == current_date and len(today) < max_row_count:
            today.append(hourly_datum)
        elif len(tonight) < max_row_count:
            tonight.append(hourly_datum)

    return {"today": today, "tonight": tonight}


def _transform_daily_data(
    weather_data: dict, lang: str, timezone: ZoneInfo, units: Units
) -> list[DailyDatum]:
    daily: list[DailyDatum] = []

    for index, day in enumerate(weather_data["daily"]):
        local_day = _convert_timestamp_to_local(day["dt"], timezone)
        weather = day["weather"][0]

        daily_datum = DailyDatum()
        daily_datum.day = _get_formatted_date(local_day, lang)
        daily_datum.moon_phase = day["moon_phase"]
        daily_datum.weather_description = weather["description"]
        daily_datum.weather_icon = weather["icon"]

        if index == 0:
            today_hourly_data = _get_hourly_data_for_today(weather_data, timezone)

            # Humidity, probability of precipitation, temperature min and max values
            # for today should be computed using only the hours which are considered
            # in our hourly report.
            daily_datum.temp_min = min(h["temp"] for h in today_hourly_data)
            daily_datum.temp_max = max(h["temp"] for h in today_hourly_data)
            daily_datum.humidity = min(h["humidity"] for h in today_hourly_data)
            daily_datum.pop = max(h["pop"] for h in today_hourly_data)

            wind = max(today_hourly_data, key=lambda h: cast(float, h["wind_speed"]))
            daily_datum.wind_speed = wind["wind_speed"]
            daily_datum.wind_deg = wind["wind_deg"]
            daily_datum.wind_gust = wind["wind_gust"] if "wind_gust" in wind else None

            # As above, precipitation for today should be computed
            # using only the hours which are considered in our hourly report.
            # For example, if it rained in the hours before our schedule,
            # then the daily datum would take that into account,
            # while it would be incorrect in our case:
            # we need to show how much precipitation there will be
            # after our schedule, not how much there has been in the whole day.
            daily_datum.prec = _get_daily_prec_from_hourly_data(today_hourly_data)
        else:
            daily_datum.temp_min = day["temp"]["min"]
            daily_datum.temp_max = day["temp"]["max"]
            daily_datum.humidity = day["humidity"]
            daily_datum.pop = day["pop"]
            daily_datum.wind_speed = day["wind_speed"]
            daily_datum.wind_deg = day["wind_deg"]
            daily_datum.wind_gust = day["wind_gust"] if "wind_gust" in day else None
            daily_datum.prec = _get_daily_prec(day)

        wind_direction = _convert_degrees_to_direction(daily_datum.wind_deg)
        daily_datum.wind_direction = f"wind_direction_{wind_direction}"
        daily_datum.wind_direction_short = f"wind_direction_{wind_direction}_short"

        daily_datum.wind_speed = _convert_wind_speed(daily_datum.wind_speed, units)
        daily_datum.wind_gust = (
            _convert_wind_speed(daily_datum.wind_gust, units)
            if daily_datum.wind_gust
            else None
        )

        daily.append(daily_datum)

    return daily


def _transform_alerts_data(
    weather_data: dict,
    lang: str,
    timezone: ZoneInfo,
    logger: logging.Logger,
) -> list[AlertDatum]:
    alerts: list[AlertDatum] = []

    if "alerts" in weather_data:
        source_alerts = weather_data["alerts"]
        logger.info("Alerts to be transformed: %d", len(source_alerts))

        for alert in source_alerts:
            local_start = _convert_timestamp_to_local(alert["start"], timezone)
            local_end = _convert_timestamp_to_local(alert["end"], timezone)
            event, icon = translate_alert_event(alert["event"], lang)

            alert_datum = AlertDatum()
            alert_datum.end_day = _get_formatted_date(local_end, lang)
            alert_datum.end_hour = local_end.strftime("%H")
            alert_datum.end_minute = local_end.strftime("%M")
            alert_datum.event = event
            alert_datum.icon = icon
            alert_datum.sender_name = alert["sender_name"]
            alert_datum.start_day = _get_formatted_date(local_start, lang)
            alert_datum.start_hour = local_start.strftime("%H")
            alert_datum.start_minute = local_start.strftime("%M")

            alerts.append(alert_datum)
    else:
        logger.info("No alerts have been received")

    return alerts


def _transform_air_pollution_data(
    hourly_data: dict, air_pollution_data: dict
) -> list[dict]:
    if len(air_pollution_data) == 0:
        return []

    today_and_tonight = hourly_data["today"] + hourly_data["tonight"]
    max_item_count = len(today_and_tonight)

    for index, item in enumerate(air_pollution_data["list"]):
        if index >= max_item_count:
            # We need to gather air pollution data only for displayed rows.
            break

        hour = today_and_tonight[index]
        hour.aqi = item["main"]["aqi"]

    air_items = []

    step = 2
    for first_index in range(0, max_item_count - 1, step):
        last_index = min(first_index + step - 1, max_item_count - 1)
        data = air_pollution_data["list"][first_index : (last_index + 1)]

        air_item = {
            "start_hour": today_and_tonight[first_index].hour,
            "end_hour": today_and_tonight[last_index].hour,
            "aqi": max((d["main"]["aqi"] for d in data), default=None),
        }

        for comp in AIR_POLLUTION_COMPONENTS:
            air_item[comp] = max((d["components"][comp] for d in data), default=None)

        air_items.append(air_item)

    return air_items


def transform_weather_data(
    request: ForecastRequest,
    weather_data: dict,
    location_data: dict,
    air_pollution_data: dict,
    logger: logging.Logger,
) -> dict:
    # OpenWeather data will be transformed in order to
    # make it easier to be handled by Jinja2 template.
    twd: dict = {}

    # Add request information.
    twd["lat"] = request.latitude
    twd["lon"] = request.longitude
    twd["lang"] = request.language
    twd["units"] = request.units.value

    # Find location name. If location name has been specified in the request,
    # then it will override the one found using reverse geocoding.
    # If local name in desired language is available,
    # then it is preferred over the "international" one.
    if request.location_name:
        location_name = request.location_name
    elif (
        "local_names" in location_data
        and request.language in location_data["local_names"]
    ):
        location_name = location_data["local_names"][request.language]
    else:
        location_name = location_data["name"]

    twd["location_name"] = location_name
    twd["location_state"] = location_data["state"] if "state" in location_data else None
    twd["location_country"] = location_data["country"]

    # Timezone will be used to convert UTC timestamps into local ones.
    timezone = ZoneInfo(weather_data["timezone"])

    twd["hourly"] = _transform_hourly_data(weather_data, timezone, request.units)
    twd["daily"] = _transform_daily_data(
        weather_data, request.language, timezone, request.units
    )
    twd["alerts"] = _transform_alerts_data(
        weather_data, request.language, timezone, logger
    )
    twd["air_items"] = _transform_air_pollution_data(twd["hourly"], air_pollution_data)

    return twd


@backoff.on_exception(
    backoff.expo,
    (httpx.HTTPError, json.JSONDecodeError),
    max_tries=MAX_HTTP_TRIES,
)
def get_weather_data(
    openweather_settings: OpenWeatherSettings,
    lat: float,
    lon: float,
    lang: str,
    units: Units,
) -> dict:
    url = (
        f"{openweather_settings.base_url}/data/3.0/onecall?exclude=current,minutely"
        f"&appid={openweather_settings.api_key}&lat={lat}&lon={lon}"
        f"&lang={lang}&units={units.value}"
    )
    response = httpx.get(url, timeout=openweather_settings.timeout)
    return cast(dict, json.loads(response.text))


@backoff.on_exception(
    backoff.expo,
    (httpx.HTTPError, json.JSONDecodeError),
    max_tries=MAX_HTTP_TRIES,
)
def get_location_data(
    openweather_settings: OpenWeatherSettings, lat: float, lon: float
) -> dict:
    url = (
        f"{openweather_settings.base_url}/geo/1.0/reverse?limit=1"
        f"&appid={openweather_settings.api_key}&lat={lat}&lon={lon}"
    )
    response = httpx.get(url, timeout=openweather_settings.timeout)
    return cast(dict, json.loads(response.text))


@backoff.on_exception(
    backoff.expo,
    (httpx.HTTPError, json.JSONDecodeError),
    max_tries=MAX_HTTP_TRIES,
)
def get_air_pollution_data(
    openweather_settings: OpenWeatherSettings, lat: float, lon: float
) -> dict:
    url = (
        f"{openweather_settings.base_url}/data/2.5/air_pollution/forecast"
        f"?appid={openweather_settings.api_key}&lat={lat}&lon={lon}"
    )
    response = httpx.get(url, timeout=openweather_settings.timeout)
    return cast(dict, json.loads(response.text))
