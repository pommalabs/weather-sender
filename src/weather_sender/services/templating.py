# Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

import os
import subprocess  # nosec B404
from tempfile import _TemporaryFileWrapper

from jinja2 import Environment, FileSystemLoader

from weather_sender.services.translations import translate

TEMPLATES_PATH = f"{os.path.dirname(__file__)}/../templates"
AIR_TEMPLATE = "air.html"
MESSAGE_TEMPLATE = "message.txt"
TODAY_TEMPLATE = "today.html"
TONIGHT_TEMPLATE = "tonight.html"
WEEK_TEMPLATE = "week.html"


def format_number(num: float | None, precision: int) -> str:
    """Rounds given number to specified precision and formats it."""
    if num is None:
        return ""
    if -1 < num <= 0 and precision == 0:
        # When number has to be truncated and it has a negative value between 0 and -1,
        # then its formatted output would be "-0". That output, despite being correct,
        # does not make much sense in a weather report. Therefore, it is formatted as "0".
        return "0"
    return f"{round(num, precision):.{precision}f}"


def get_resource_file_uri(rel_path: str) -> str:
    """Builds an absolute resource file URI, starting from templates path."""
    return f"file://{TEMPLATES_PATH}{rel_path}"


_jinja2_env = Environment(loader=FileSystemLoader(TEMPLATES_PATH), autoescape=True)

# Filters:
_jinja2_env.filters["transl"] = translate
_jinja2_env.filters["break"] = lambda txt: txt.replace(" ", "<br/>")
_jinja2_env.filters["fileuri"] = get_resource_file_uri
_jinja2_env.filters["numfmt"] = format_number


def render_template(name: str, data: dict) -> str:
    template = _jinja2_env.get_template(name)
    return template.render(data)


def convert_html_to_image(
    html_contents: str,
    html_file: _TemporaryFileWrapper,
    image_file: _TemporaryFileWrapper,
) -> None:
    html_file.write(html_contents)
    html_file.flush()

    subprocess.check_call(  # nosec B603, B607
        [
            "firefox",
            "--headless",
            "--screenshot",
            image_file.name,
            "--window-size",
            "630,840",
            "file://" + html_file.name,
        ]
    )
