# Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

EN = "en"
IT = "it"

ALERT_ICON = "icon"

LABELS = {
    # Words and sentences:
    "air": {
        EN: "Air",
        IT: "Aria",
    },
    "air_quality_index_short": {
        EN: "Air",
        IT: "Aria",
    },
    "alert_source": {
        EN: "Source",
        IT: "Fonte",
    },
    "alert_validity": {
        EN: "Validity",
        IT: "Validità",
    },
    "forecast": {
        EN: "Forecast",
        IT: "Previsione",
    },
    "humidity": {
        EN: "Humidity",
        IT: "Umidità",
    },
    "humidity_short": {
        EN: "Hmd",
        IT: "Umid",
    },
    "humidity_higher_than": {
        EN: "higher than",
        IT: "superiore al",
    },
    "moon_phase_short": {
        EN: "Moon",
        IT: "Luna",
    },
    "no_prec": {
        EN: "no",
        IT: "assenti",
    },
    "no_or_light_prec": {
        EN: "no or light",
        IT: "assenti o lievi",
    },
    "openweather_attribution": {
        EN: "Weather data provided by OpenWeather (TM)",
        IT: "Dati meteo forniti da OpenWeather (TM)",
    },
    "pollutant_co": {
        EN: "Carbon monoxide",
        IT: "Monossido di carbonio",
    },
    "pollutant_no2": {
        EN: "Nitrogen dioxide",
        IT: "Biossido di azoto",
    },
    "pollutant_o3": {
        EN: "Ozone",
        IT: "Ozono",
    },
    "pollutant_pm10": {
        EN: "Particulate",
        IT: "Particolato",
    },
    "pollutant_pm2_5": {
        EN: "Particulate",
        IT: "Particolato",
    },
    "pollutant_so2": {
        EN: "Sulfur dioxide",
        IT: "Biossido di zolfo",
    },
    "prec": {
        EN: "Precipitations",
        IT: "Precipitazioni",
    },
    "prec_short": {
        EN: "Prcp",
        IT: "Prec",
    },
    "prec_probability": {
        EN: "probability",
        IT: "di probabilità",
    },
    "quality_short": {
        EN: "Qual",
        IT: "Qual",
    },
    "temp": {
        EN: "Temperature",
        IT: "Temperatura",
    },
    "temp_short": {
        EN: "Temp",
        IT: "Temp",
    },
    "today": {
        EN: "Today",
        IT: "Oggi",
    },
    "tonight": {
        EN: "Tonight",
        IT: "Stanotte",
    },
    "uv_index_short": {
        EN: "&nbsp;UV&nbsp;",
        IT: "&nbsp;UV&nbsp;",
    },
    "values_expressed_in": {
        EN: "All values are expressed in",
        IT: "Tutti i valori sono espressi in",
    },
    "week": {
        EN: "Week",
        IT: "Settimana",
    },
    "wind": {
        EN: "Wind",
        IT: "Vento",
    },
    "wind_short": {
        EN: "Wind",
        IT: "Vento",
    },
    "wind_from": {
        EN: "from",
        IT: "da",
    },
    "wind_speed_up_to": {
        EN: "up to",
        IT: "fino a",
    },
    # Wind directions:
    "wind_direction_N": {
        EN: "north",
        IT: "nord (Tramontana)",
    },
    "wind_direction_N_short": {
        EN: "N",
        IT: "N",
    },
    "wind_direction_NNE": {
        EN: "north/north-east",
        IT: "nord/nord-est (Bora)",
    },
    "wind_direction_NNE_short": {
        EN: "N/NE",
        IT: "N/NE",
    },
    "wind_direction_NE": {
        EN: "north-east",
        IT: "nord-est (Grecale)",
    },
    "wind_direction_NE_short": {
        EN: "NE",
        IT: "NE",
    },
    "wind_direction_ENE": {
        EN: "east/north-east",
        IT: "est/nord-est (Schiavo)",
    },
    "wind_direction_ENE_short": {
        EN: "E/NE",
        IT: "E/NE",
    },
    "wind_direction_E": {
        EN: "east",
        IT: "est (Levante)",
    },
    "wind_direction_E_short": {
        EN: "E",
        IT: "E",
    },
    "wind_direction_ESE": {
        EN: "east/south-east",
        IT: "est/sud-est (Solano)",
    },
    "wind_direction_ESE_short": {
        EN: "E/SE",
        IT: "E/SE",
    },
    "wind_direction_SE": {
        EN: "south-east",
        IT: "sud-est (Scirocco)",
    },
    "wind_direction_SE_short": {
        EN: "SE",
        IT: "SE",
    },
    "wind_direction_SSE": {
        EN: "south/south-east",
        IT: "sud/sud-est (Africo)",
    },
    "wind_direction_SSE_short": {
        EN: "S/SE",
        IT: "S/SE",
    },
    "wind_direction_S": {
        EN: "south",
        IT: "sud (Ostro)",
    },
    "wind_direction_S_short": {
        EN: "S",
        IT: "S",
    },
    "wind_direction_SSW": {
        EN: "south/south-west",
        IT: "sud/sud-ovest (Gauro)",
    },
    "wind_direction_SSW_short": {
        EN: "S/SW",
        IT: "S/SO",
    },
    "wind_direction_SW": {
        EN: "south-west",
        IT: "sud-ovest (Libeccio)",
    },
    "wind_direction_SW_short": {
        EN: "SW",
        IT: "SO",
    },
    "wind_direction_WSW": {
        EN: "west/south-west",
        IT: "ovest/sud-ovest (Etesia)",
    },
    "wind_direction_WSW_short": {
        EN: "W/SW",
        IT: "O/SO",
    },
    "wind_direction_W": {
        EN: "west",
        IT: "ovest (Ponente)",
    },
    "wind_direction_W_short": {
        EN: "W",
        IT: "O",
    },
    "wind_direction_WNW": {
        EN: "west/north-west",
        IT: "ovest/nord-ovest (Traversone)",
    },
    "wind_direction_WNW_short": {
        EN: "W/NW",
        IT: "O/NO",
    },
    "wind_direction_NW": {
        EN: "north-west",
        IT: "nord-ovest (Maestrale)",
    },
    "wind_direction_NW_short": {
        EN: "NW",
        IT: "NO",
    },
    "wind_direction_NNW": {
        EN: "north/north-west",
        IT: "nord/nord-ovest (Zefiro)",
    },
    "wind_direction_NNW_short": {
        EN: "N/NW",
        IT: "N/NO",
    },
    # Alert sources:
    "Italian Air Force National Meteorological Service": {
        IT: "Aeronautica Militare",
    },
    # Alert events:
    "orange high-temperature warning": {
        ALERT_ICON: "🟠",
        EN: "Orange high-temperature warning",
        IT: "Allerta arancione per temperature elevate",
    },
    "orange rain warning": {
        ALERT_ICON: "🟠",
        EN: "Orange rain warning",
        IT: "Allerta arancione per pioggia",
    },
    "orange snow-ice warning": {
        ALERT_ICON: "🟠",
        EN: "Orange snow/ice warning",
        IT: "Allerta arancione per neve/ghiaccio",
    },
    "orange thunderstorm warning": {
        ALERT_ICON: "🟠",
        EN: "Orange thunderstorm warning",
        IT: "Allerta arancione per temporali",
    },
    "orange wind warning": {
        ALERT_ICON: "🟠",
        EN: "Orange wind warning",
        IT: "Allerta arancione per vento",
    },
    "red high-temperature warning": {
        ALERT_ICON: "🔴",
        EN: "Red high-temperature warning",
        IT: "Allerta rossa per temperature elevate",
    },
    "red rain warning": {
        ALERT_ICON: "🔴",
        EN: "Red rain warning",
        IT: "Allerta rossa per pioggia",
    },
    "red snow-ice warning": {
        ALERT_ICON: "🔴",
        EN: "Red snow/ice warning",
        IT: "Allerta rossa per neve/ghiaccio",
    },
    "red thunderstorm warning": {
        ALERT_ICON: "🔴",
        EN: "Red thunderstorm warning",
        IT: "Allerta rossa per temporali",
    },
    "red wind warning": {
        ALERT_ICON: "🔴",
        EN: "Red wind warning",
        IT: "Allerta rossa per vento",
    },
    "yellow fog warning": {
        ALERT_ICON: "🟡",
        EN: "Yellow fog warning",
        IT: "Allerta gialla per nebbia",
    },
    "yellow high-temperature warning": {
        ALERT_ICON: "🟡",
        EN: "Yellow high-temperature warning",
        IT: "Allerta gialla per temperature elevate",
    },
    "yellow rain warning": {
        ALERT_ICON: "🟡",
        EN: "Yellow rain warning",
        IT: "Allerta gialla per pioggia",
    },
    "yellow snow-ice warning": {
        ALERT_ICON: "🟡",
        EN: "Yellow snow/ice warning",
        IT: "Allerta gialla per neve/ghiaccio",
    },
    "yellow thunderstorm warning": {
        ALERT_ICON: "🟡",
        EN: "Yellow thunderstorm warning",
        IT: "Allerta gialla per temporali",
    },
    "yellow wind warning": {
        ALERT_ICON: "🟡",
        EN: "Yellow wind warning",
        IT: "Allerta gialla per vento",
    },
}


def translate(label: str, lang: str) -> str:
    if label in LABELS and lang in LABELS[label]:
        return LABELS[label][lang]
    return label


def translate_alert_event(event: str, lang: str) -> tuple[str, str]:
    label = event.lower()
    icon = "⚠"
    if label in LABELS:
        if lang in LABELS[label]:
            event = LABELS[label][lang]
        if ALERT_ICON in LABELS[label]:
            icon = LABELS[label][ALERT_ICON]
    return event, icon
