# Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

from typing import cast

from dolcetto import ApiKeyAuthenticationMiddleware
from dolcetto.event_handlers import warn_if_security_disabled
from litestar.middleware import DefineMiddleware
from litestar.testing import TestClient

from weather_sender.main import app, restricted_api_v1

################################################################################
# Configuration
################################################################################


def test_that_app_can_be_loaded():
    # Act
    with TestClient(app=app) as client:
        # Assert
        assert client


def test_that_restricted_api_v1_router_enforces_authentication():
    # Assert
    assert any(
        filter(
            lambda m: cast(DefineMiddleware, m).middleware
            is ApiKeyAuthenticationMiddleware,
            restricted_api_v1.middleware,
        )
    )


def test_that_app_has_expected_startup_handlers():
    # Assert
    assert len(app.on_startup) == 1
    assert warn_if_security_disabled in app.on_startup


def test_that_app_has_expected_shutdown_handlers():
    # Assert
    assert len(app.on_shutdown) == 0
