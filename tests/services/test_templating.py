# Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

import string
import subprocess
from unittest.mock import MagicMock, patch
from uuid import UUID

import pytest
from hypothesis import given
from hypothesis import strategies as st

from tests.utils import (
    AIR_POLLUTION_SAMPLES,
    LOCATION_SAMPLES,
    WEATHER_SAMPLES,
    district_name_strategy,
    language_strategy,
    transform_weather_data,
)
from weather_sender.services.templating import (
    AIR_TEMPLATE,
    MESSAGE_TEMPLATE,
    TODAY_TEMPLATE,
    TONIGHT_TEMPLATE,
    WEEK_TEMPLATE,
    convert_html_to_image,
    format_number,
    get_resource_file_uri,
    render_template,
)

TEMPLATES = [
    AIR_TEMPLATE,
    MESSAGE_TEMPLATE,
    TODAY_TEMPLATE,
    TONIGHT_TEMPLATE,
    WEEK_TEMPLATE,
]


################################################################################
# Render template
################################################################################


# pylint: disable=duplicate-code,too-many-arguments,too-many-positional-arguments
@given(
    st.sampled_from(WEATHER_SAMPLES),
    st.sampled_from(LOCATION_SAMPLES),
    district_name_strategy(),
    st.sampled_from(AIR_POLLUTION_SAMPLES),
    language_strategy(),
    st.sampled_from(TEMPLATES),
)
def test_that_render_template_returns_non_empty_string(
    weather_sample: str,
    location_sample: str,
    district_name: str | None,
    air_pollution_sample: str,
    lang: str,
    template_name: str,
):
    # Arrange
    weather_data = transform_weather_data(
        weather_sample,
        location_sample,
        district_name,
        air_pollution_sample,
        lang,
    )
    # Act
    rendered = render_template(template_name, weather_data)
    # Assert
    assert len(rendered) > 0


################################################################################
# Convert HTML to PNG
################################################################################


@given(st.text(), st.uuids(), st.uuids())
def test_that_convert_html_to_image_invokes_chromium(
    html_contents: str,
    html_file_name: UUID,
    image_file_name: UUID,
):
    # Arrange
    html_file = MagicMock()
    html_file.name = str(html_file_name)
    image_file = MagicMock()
    image_file.name = str(image_file_name)
    with patch.object(subprocess, "check_call") as mock:
        # Act
        convert_html_to_image(html_contents, html_file, image_file)
        # Assert
        mock.assert_called_once()
        args = mock.call_args.args[0]
        assert args[0] == "firefox"
        assert any(filter(lambda arg: html_file.name in str(arg), args))
        assert any(filter(lambda arg: image_file.name in str(arg), args))


################################################################################
# Filters
################################################################################


@pytest.mark.parametrize(
    "number,precision,expected_output",
    [
        pytest.param(None, 0, ""),
        pytest.param(0, 0, "0"),
        pytest.param(-0.3, 0, "0"),
        pytest.param(+0.3, 0, "0"),
        pytest.param(-1, 0, "-1"),
        pytest.param(None, 1, ""),
        pytest.param(0, 1, "0.0"),
        pytest.param(-0.3, 1, "-0.3"),
        pytest.param(+0.3, 1, "0.3"),
        pytest.param(-1, 1, "-1.0"),
        pytest.param(-1.23, 1, "-1.2"),
    ],
)
def test_that_format_number_behaves_as_expected(
    number: float | None, precision: int, expected_output: str
):
    # Act
    output = format_number(number, precision)
    # Assert
    assert output == expected_output


@given(st.text(alphabet=string.printable))
def test_that_get_resource_file_uri_contains_required_parts(rel_path: str):
    # Act
    resource_file_uri = get_resource_file_uri(rel_path)
    # Assert
    assert resource_file_uri.startswith("file://")
    assert resource_file_uri.endswith(rel_path)
