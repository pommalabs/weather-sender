# Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

from unittest.mock import MagicMock, patch
from urllib.parse import parse_qs, urlparse

import httpx
from hypothesis import given
from hypothesis import strategies as st

from tests.utils import (
    AIR_POLLUTION_SAMPLES,
    LOCATION_SAMPLES,
    WEATHER_SAMPLES,
    api_key_strategy,
    district_name_strategy,
    get_stub_openweather_settings,
    language_strategy,
    latitude_strategy,
    load_sample,
    longitude_strategy,
    transform_weather_data,
)
from weather_sender.models import Units
from weather_sender.services.openweather import (
    MAX_HTTP_TRIES,
    get_air_pollution_data,
    get_location_data,
    get_weather_data,
)
from weather_sender.settings import OpenWeatherSettings

API_KEY_LENGTH = 32


def get_fake_response(text: str):
    response = MagicMock()
    response.text = text
    return response


EMPTY_RESPONSE = get_fake_response("{}")

################################################################################
# Process weather data
################################################################################


@given(
    st.sampled_from(WEATHER_SAMPLES),
    st.sampled_from(LOCATION_SAMPLES),
    district_name_strategy(),
    st.sampled_from(AIR_POLLUTION_SAMPLES),
    language_strategy(),
)
def test_that_process_weather_data_returns_a_dict(
    weather_sample: str,
    location_sample: str,
    district_name: str | None,
    air_pollution_sample: str,
    lang: str,
):
    # Act
    twd = transform_weather_data(
        weather_sample,
        location_sample,
        district_name,
        air_pollution_sample,
        lang,
    )
    # Assert
    assert isinstance(twd, dict)


################################################################################
# Get weather data
################################################################################


@given(
    api_key_strategy(),
    latitude_strategy(),
    longitude_strategy(),
    language_strategy(),
    st.sampled_from(Units),
)
def test_that_get_weather_data_uses_input_parameters(
    api_key: str, lat: float, lon: float, lang: str, units: Units
):
    # Arrange
    openweather_settings = OpenWeatherSettings(api_key=api_key)
    with patch.object(httpx, "get", return_value=EMPTY_RESPONSE) as mock:
        # Act
        get_weather_data(openweather_settings, lat, lon, lang, units)
        # Assert
        mock.assert_called_once()
        url = urlparse(mock.call_args.args[0])
        query = parse_qs(url.query)
        assert query["appid"][0] == api_key
        assert query["lat"][0] == str(lat)
        assert query["lon"][0] == str(lon)
        assert query["lang"][0] == str(lang)


# pylint: disable=too-many-arguments,too-many-positional-arguments
@given(
    api_key_strategy(),
    latitude_strategy(),
    longitude_strategy(),
    language_strategy(),
    st.sampled_from(Units),
    st.sampled_from(WEATHER_SAMPLES),
)
def test_that_get_weather_data_parses_json_response(
    api_key: str, lat: float, lon: float, lang: str, units: Units, sample: str
):
    # Arrange
    openweather_settings = OpenWeatherSettings(api_key=api_key)
    (response_text, parsed_response) = load_sample(sample)
    with patch.object(httpx, "get", return_value=get_fake_response(response_text)):
        # Act
        weather_data = get_weather_data(openweather_settings, lat, lon, lang, units)
        # Assert
        assert weather_data == parsed_response


# Hypothesis is not used here, because its retries, combined with the retries
# which are tested here, would make this test extremely slow and unreliable.
def test_that_get_weather_data_retries_on_error():
    # Arrange
    openweather_settings = get_stub_openweather_settings()
    with patch.object(httpx, "get", side_effect=httpx.TimeoutException("")) as mock:
        # Act
        try:
            get_weather_data(openweather_settings, 0, 0, "en", Units.METRIC)
        except httpx.TimeoutException:
            pass
        # Assert
        assert mock.call_count == MAX_HTTP_TRIES


################################################################################
# Get location data
################################################################################


@given(
    api_key_strategy(),
    latitude_strategy(),
    longitude_strategy(),
)
def test_that_get_location_data_uses_input_parameters(
    api_key: str, lat: float, lon: float
):
    # Arrange
    openweather_settings = OpenWeatherSettings(api_key=api_key)
    with patch.object(httpx, "get", return_value=EMPTY_RESPONSE) as mock:
        # Act
        get_location_data(openweather_settings, lat, lon)
        # Assert
        mock.assert_called_once()
        url = urlparse(mock.call_args.args[0])
        query = parse_qs(url.query)
        assert query["appid"][0] == api_key
        assert query["lat"][0] == str(lat)
        assert query["lon"][0] == str(lon)


@given(
    api_key_strategy(),
    latitude_strategy(),
    longitude_strategy(),
    st.sampled_from(LOCATION_SAMPLES),
)
def test_that_get_location_data_parses_json_response(
    api_key: str, lat: float, lon: float, sample: str
):
    # Arrange
    openweather_settings = OpenWeatherSettings(api_key=api_key)
    (response_text, parsed_response) = load_sample(sample)
    with patch.object(httpx, "get", return_value=get_fake_response(response_text)):
        # Act
        weather_data = get_location_data(openweather_settings, lat, lon)
        # Assert
        assert weather_data == parsed_response


# Hypothesis is not used here, because its retries, combined with the retries
# which are tested here, would make this test extremely slow and unreliable.
def test_that_get_location_data_retries_on_error():
    # Arrange
    openweather_settings = get_stub_openweather_settings()
    with patch.object(httpx, "get", side_effect=httpx.TimeoutException("")) as mock:
        # Act
        try:
            get_location_data(openweather_settings, 0, 0)
        except httpx.TimeoutException:
            pass
        # Assert
        assert mock.call_count == MAX_HTTP_TRIES


################################################################################
# Get air pollution data
################################################################################


@given(
    api_key_strategy(),
    latitude_strategy(),
    longitude_strategy(),
)
def test_that_get_air_pollution_data_uses_input_parameters(
    api_key: str, lat: float, lon: float
):
    # Arrange
    openweather_settings = OpenWeatherSettings(api_key=api_key)
    with patch.object(httpx, "get", return_value=EMPTY_RESPONSE) as mock:
        # Act
        get_air_pollution_data(openweather_settings, lat, lon)
        # Assert
        mock.assert_called_once()
        url = urlparse(mock.call_args.args[0])
        query = parse_qs(url.query)
        assert query["appid"][0] == api_key
        assert query["lat"][0] == str(lat)
        assert query["lon"][0] == str(lon)


@given(
    api_key_strategy(),
    latitude_strategy(),
    longitude_strategy(),
    st.sampled_from(AIR_POLLUTION_SAMPLES),
)
def test_that_get_air_pollution_data_parses_json_response(
    api_key: str, lat: float, lon: float, sample: str
):
    # Arrange
    openweather_settings = OpenWeatherSettings(api_key=api_key)
    (response_text, parsed_response) = load_sample(sample)
    with patch.object(httpx, "get", return_value=get_fake_response(response_text)):
        # Act
        weather_data = get_air_pollution_data(openweather_settings, lat, lon)
        # Assert
        assert weather_data == parsed_response


# Hypothesis is not used here, because its retries, combined with the retries
# which are tested here, would make this test extremely slow and unreliable.
def test_that_get_air_pollution_data_retries_on_error():
    # Arrange
    openweather_settings = get_stub_openweather_settings()
    with patch.object(httpx, "get", side_effect=httpx.TimeoutException("")) as mock:
        # Act
        try:
            get_air_pollution_data(openweather_settings, 0, 0)
        except httpx.TimeoutException:
            pass
        # Assert
        assert mock.call_count == MAX_HTTP_TRIES
