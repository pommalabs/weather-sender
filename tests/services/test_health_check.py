# Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

from unittest.mock import patch

from dolcetto.services import get_logger

from tests.utils import get_stub_openweather_settings
from weather_sender.services import get_health_check
from weather_sender.services import health_check as health_check_module
from weather_sender.settings import HealthCheckSettings, OpenWeatherSettings


def _health_check_factory(
    health_check_settings: HealthCheckSettings = HealthCheckSettings.load(),
    openweather_settings: OpenWeatherSettings = get_stub_openweather_settings(),
):
    return get_health_check(
        logger=get_logger(),
        health_check_settings=health_check_settings,
        openweather_settings=openweather_settings,
    )


################################################################################
# Probe OpenWeather API
################################################################################


def test_that_health_check_invokes_probe_url_when_probing_openweather_api():
    # Arrange
    health_check = _health_check_factory()
    with patch.object(
        health_check_module, "probe_url", return_value=(True, "")
    ) as mock:
        # Act
        health_check.run()
        # Assert
        mock.assert_called_once()
