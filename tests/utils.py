# Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

import json
import string
from os import path

from dolcetto.services import get_logger
from hypothesis import strategies as st

from weather_sender.models import ForecastRequest
from weather_sender.services import openweather
from weather_sender.services.translations import EN, IT
from weather_sender.settings import OpenWeatherSettings, TelegramSettings

STUB_OPENWEATHER_API_KEY = "API_KEY"
STUB_TELEGRAM_BOT_TOKEN = "BOT_TOKEN"

SAMPLES_DIR = "./tests/samples/"
WEATHER_SAMPLES = [
    "onecall_negative_zero.json",
    "onecall_with_alerts.json",
    "onecall_with_snow.json",
    "onecall_without_alerts.json",
    "onecall_without_prec.json",
    "onecall_without_wind_gust.json",
]
LOCATION_SAMPLES = [
    "reverse_with_local_names.json",
    "reverse_without_local_names.json",
    "reverse_without_state.json",
]
AIR_POLLUTION_SAMPLES = [
    "air_pollution_empty.json",
    "air_pollution_full.json",
]


def get_stub_openweather_settings():
    return OpenWeatherSettings(api_key=STUB_OPENWEATHER_API_KEY)


def get_stub_telegram_settings():
    return TelegramSettings(bot_token=STUB_TELEGRAM_BOT_TOKEN)


def load_sample(sample):
    with open(
        path.join(SAMPLES_DIR, sample), mode="r", encoding="utf-8"
    ) as sample_file:
        sample_json = sample_file.read()
        parsed_sample = json.loads(sample_json)
        return sample_json, parsed_sample


def transform_weather_data(
    weather_sample: str,
    location_sample: str,
    district_name: str | None,
    air_pollution_sample: str,
    lang: str,
):
    (_, weather_data) = load_sample(weather_sample)
    (_, location_data) = load_sample(location_sample)
    (_, air_pollution_data) = load_sample(air_pollution_sample)
    return openweather.transform_weather_data(
        ForecastRequest(
            latitude=0,
            longitude=0,
            language=lang,
            chat_id=0,
            location_name=district_name,
        ),
        weather_data,
        location_data[0],
        air_pollution_data,
        get_logger(),
    )


@st.composite
def api_key_strategy(draw: st.DrawFn):
    api_key_len = 32
    return draw(
        st.text(
            alphabet=string.ascii_letters, min_size=api_key_len, max_size=api_key_len
        )
    )


@st.composite
def latitude_strategy(draw: st.DrawFn):
    return draw(st.floats(min_value=-90, max_value=+90))


@st.composite
def longitude_strategy(draw: st.DrawFn):
    return draw(st.floats(min_value=-180, max_value=+180))


@st.composite
def language_strategy(draw: st.DrawFn):
    return draw(st.sampled_from([EN, IT]))


@st.composite
def forecast_request_strategy(draw: st.DrawFn):
    return draw(
        st.builds(
            ForecastRequest,
            latitude=latitude_strategy(),
            longitude=longitude_strategy(),
            language=language_strategy(),
            chat_id=st.integers(),
        )
    )


@st.composite
def district_name_strategy(draw: st.DrawFn):
    return draw(st.text(alphabet=string.printable))
