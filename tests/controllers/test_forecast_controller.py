# Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

from unittest.mock import AsyncMock, MagicMock, patch

from dolcetto.services import SmtpClient, get_logger
from hypothesis import given
from hypothesis import strategies as st
from litestar.di import Provide
from litestar.status_codes import HTTP_200_OK
from litestar.testing import create_test_client

from tests.utils import (
    AIR_POLLUTION_SAMPLES,
    LOCATION_SAMPLES,
    WEATHER_SAMPLES,
    forecast_request_strategy,
    get_stub_openweather_settings,
    get_stub_telegram_settings,
    load_sample,
)
from weather_sender.controllers import forecast_controller
from weather_sender.models import (
    ForecastRequest,
    ForecastRequests,
    ForecastResult,
    ForecastSourceData,
    ForecastTransformedData,
)

CONTROLLER_DEPENDENCIES = {
    "logger": Provide(get_logger, sync_to_thread=False),
    "openweather_settings": Provide(
        get_stub_openweather_settings, sync_to_thread=False
    ),
    "telegram_settings": Provide(get_stub_telegram_settings, sync_to_thread=False),
    "smtp_client": Provide(
        lambda: MagicMock(spec_set=SmtpClient), sync_to_thread=False
    ),
}

################################################################################
# Send one weather forecast
################################################################################

SEND_ONE_ENDPOINT = "/forecasts/send-one"


@given(
    forecast_request_strategy(),
    st.sampled_from(LOCATION_SAMPLES),
    st.sampled_from(WEATHER_SAMPLES),
    st.sampled_from(AIR_POLLUTION_SAMPLES),
)
def test_that_send_one_weather_forecast_endpoint_reads_openweather_data(
    request: ForecastRequest,
    location_sample: str,
    weather_sample: str,
    air_pollution_sample: str,
):
    # Arrange
    (_, location_data) = load_sample(location_sample)
    (_, weather_data) = load_sample(weather_sample)
    (_, air_pollution_data) = load_sample(air_pollution_sample)
    with (
        create_test_client(
            route_handlers=forecast_controller.ForecastController,
            dependencies=CONTROLLER_DEPENDENCIES,
        ) as client,
        patch.object(
            forecast_controller,
            "get_location_data",
            return_value=location_data,
        ) as mock1,
        patch.object(
            forecast_controller,
            "get_weather_data",
            return_value=weather_data,
        ) as mock2,
        patch.object(
            forecast_controller,
            "get_air_pollution_data",
            return_value=air_pollution_data,
        ) as mock3,
        patch.object(forecast_controller, "send_telegram_message"),
    ):
        # Act
        response = client.post(SEND_ONE_ENDPOINT, json=request.dict())
        # Assert
        assert response.status_code == HTTP_200_OK
        mock1.assert_called_once()
        mock2.assert_called_once()
        mock3.assert_called_once()


@given(
    forecast_request_strategy(),
    st.sampled_from(LOCATION_SAMPLES),
    st.sampled_from(WEATHER_SAMPLES),
    st.sampled_from(AIR_POLLUTION_SAMPLES),
)
def test_that_send_one_weather_forecast_endpoint_sends_telegram_message(
    request: ForecastRequest,
    location_sample: str,
    weather_sample: str,
    air_pollution_sample: str,
):
    # Arrange
    (_, location_data) = load_sample(location_sample)
    (_, weather_data) = load_sample(weather_sample)
    (_, air_pollution_data) = load_sample(air_pollution_sample)
    with (
        create_test_client(
            route_handlers=forecast_controller.ForecastController,
            dependencies=CONTROLLER_DEPENDENCIES,
        ) as client,
        patch.object(
            forecast_controller,
            "get_location_data",
            return_value=location_data,
        ),
        patch.object(
            forecast_controller,
            "get_weather_data",
            return_value=weather_data,
        ),
        patch.object(
            forecast_controller,
            "get_air_pollution_data",
            return_value=air_pollution_data,
        ),
        patch.object(forecast_controller, "send_telegram_message") as mock,
    ):
        # Act
        response = client.post(SEND_ONE_ENDPOINT, json=request.dict())
        # Assert
        assert response.status_code == HTTP_200_OK
        mock.assert_called_once()


################################################################################
# Send many weather forecasts
################################################################################

SEND_MANY_ENDPOINT = "/forecasts/send-many"

EMPTY_FORECAST_RESULT = ForecastResult(
    source_data=ForecastSourceData(weather={}, location={}, air_pollution={}),
    transformed_data=ForecastTransformedData(weather={}),
)


@given(st.lists(forecast_request_strategy()))
def test_that_send_many_weather_forecasts_endpoint_uses_all_inputs(
    requests: list[ForecastRequest],
):
    # Arrange
    with (
        create_test_client(
            route_handlers=forecast_controller.ForecastController,
            dependencies=CONTROLLER_DEPENDENCIES,
        ) as client,
        patch.object(
            forecast_controller.ForecastController,
            "_do_send_one_weather_forecast",
            side_effect=AsyncMock(return_value=EMPTY_FORECAST_RESULT),
        ) as mock,
    ):
        # Act
        response = client.post(
            SEND_MANY_ENDPOINT,
            json=ForecastRequests(requests=requests, background=False).dict(),
        )
        # Assert
        assert response.status_code == HTTP_200_OK
        results = response.json()["results"]
        assert len(results) == len(requests)
        assert mock.call_count == len(requests)


@given(st.lists(forecast_request_strategy()))
def test_that_send_many_weather_forecasts_queues_all_inputs_when_background_true(
    requests: list[ForecastRequest],
):
    # Arrange
    with (
        create_test_client(
            route_handlers=forecast_controller.ForecastController,
            dependencies=CONTROLLER_DEPENDENCIES,
        ) as client,
        patch.object(
            forecast_controller.ForecastController, "add_background_task"
        ) as mock,
    ):
        # Act
        response = client.post(
            SEND_MANY_ENDPOINT,
            json=ForecastRequests(requests=requests, background=True).dict(),
        )
        # Assert
        assert response.status_code == HTTP_200_OK
        results = response.json()["results"]
        assert len(results) == 0
        assert mock.call_count == len(requests)
