# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [4] - 2023-08-09

### Changed

- `HEALTH_CHECK_PING_TIMEOUT` configuration has been renamed to `HEALTH_CHECK_PROBE_TIMEOUT`.

## [3] - 2022-04-25

### Added

- Added UV index.

## [2] - 2022-04-20

### Added

- Added unit tests.

## [1] - 2022-01-06

### Added

- Initial release.

[4]: https://gitlab.com/pommalabs/weather-sender/-/compare/3...4
[3]: https://gitlab.com/pommalabs/weather-sender/-/compare/2...3
[2]: https://gitlab.com/pommalabs/weather-sender/-/compare/1...2
[1]: https://gitlab.com/pommalabs/weather-sender/-/tags/1
